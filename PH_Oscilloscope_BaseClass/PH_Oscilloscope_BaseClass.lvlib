﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_Oscilloscope_BaseClass.calc Status.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.calc Status.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.check Error.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.check Error.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Clear.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Clear.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Close.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Close.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Configure Acquisition.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Configure Acquisition.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Configure Channel.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Configure Channel.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Configure Trigger.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Configure Trigger.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Fetch Waveform.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Fetch Waveform.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.get Channel Name.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.get Channel Name.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.i attribute.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.i attribute.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.i attribute.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.i attribute.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Init.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Init.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Freeze Scaling.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Freeze Scaling.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Lock.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Lock.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Separate Waveforms.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Separate Waveforms.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Set Channel Position.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Set Channel Position.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Set Display Intensity.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Set Display Intensity.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Set Remote Control.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Set Remote Control.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.ProcEvents.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.ProcEvents.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Reset.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Reset.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Revision.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Revision.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Selftest.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Selftest.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set Acquisition GUI Element.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set Acquisition GUI Element.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Set Bin Data Encoding.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Set Bin Data Encoding.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set Channel GUI Element.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set Channel GUI Element.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set initial i Attributes.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set initial i Attributes.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set Trigger GUI Element.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set Trigger GUI Element.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set Y axes scalings.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set Y axes scalings.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.start Communication.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.start Communication.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_Oscilloscope_BaseClass.get i attribute.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.get i attribute.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.ProcCases.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.ProcCases.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.ProcPeriodic.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.ProcPeriodic.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set GUI Refs.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set GUI Refs.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set i attribute.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_Oscilloscope_BaseClass.Acquisition Configuration.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.Acquisition Configuration.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.Allow Prdc Loop.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Allow Prdc Loop.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Channel Configuration.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.Channel Configuration.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.Channel Settings.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.Channel Settings.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.Configure Hardware.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Configure Hardware.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.constructor.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.constructor.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.destructor.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.destructor.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Display Position.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.Display Position.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Clear.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Clear.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Close.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Close.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Fetch all.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Fetch all.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Freeze Scaling.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Freeze Scaling.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Init.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Init.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Lock.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Lock.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Reset.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Reset.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Selftest.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Selftest.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Separate Waveforms.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Separate Waveforms.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Set Acquisition Configuration.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Set Acquisition Configuration.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Set Channel Configuration.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Set Channel Configuration.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Set Channel Position.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Set Channel Position.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Set Display Intensity.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Set Display Intensity.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Set Remote Control.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Set Remote Control.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Evt Call Set Trigger Configuration.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Evt Call Set Trigger Configuration.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Fetch all Waveforms.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Fetch all Waveforms.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.get data to modify.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.get data to modify.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.GUI Refs.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.GUI Refs.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.Indicate all Waveforms.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Indicate all Waveforms.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.pack PSDB Data.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.pack PSDB Data.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.panel.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.panel.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Set Channel Name.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Set Channel Name.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.PC Write PSDB.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.PC Write PSDB.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Save Data to Harddisk.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Save Data to Harddisk.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set modified data.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set modified data.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.set no periodic fetching.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.set no periodic fetching.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Set Plot Legend.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.Set Plot Legend.vi"/>
		<Item Name="PH_Oscilloscope_BaseClass.Trigger Configuration.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.Trigger Configuration.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.Waveform Limits.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.Waveform Limits.ctl"/>
		<Item Name="PH_Oscilloscope_BaseClass.Waveform to fetch.ctl" Type="VI" URL="../PH_Oscilloscope_BaseClass.Waveform to fetch.ctl"/>
	</Item>
	<Item Name="PH_Oscilloscope_BaseClass.contents.vi" Type="VI" URL="../PH_Oscilloscope_BaseClass.contents.vi"/>
</Library>
