﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.Decor Lens in Shotmode.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.Decor Lens in Shotmode.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.Decor MM1 Bend.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.Decor MM1 Bend.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.Decor MM1.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.Decor MM1.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.Decor PA Aperture Selector.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.Decor PA Aperture Selector.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.decorate GUI elementss.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.decorate GUI elementss.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.i attribute.ctl" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.i attribute.ctl"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.i attribute.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.i attribute.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.PC _4Q Position.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.PC _4Q Position.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.set Tip Strip.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.set Tip Strip.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.get i attribute.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.get i attribute.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.get Map Value.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.get Map Value.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.ProcCases.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.ProcCases.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.ProcEvents.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.ProcEvents.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.ProcPeriodic.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.ProcPeriodic.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.set GUI Refs.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.set GUI Refs.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.set i attribute.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.set i attribute.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.set Subscribers.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.set Subscribers.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.4 Pos Selector Instances.ctl" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.4 Pos Selector Instances.ctl"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.constructor.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.constructor.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.destructor.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.destructor.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.get data to modify.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.get data to modify.vi"/>
		<Item Name="PH_Handle-4Q-Subscriber_BaseClass.set modified data.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.set modified data.vi"/>
	</Item>
	<Item Name="PH_Handle-4Q-Subscriber_BaseClass.contents.vi" Type="VI" URL="../PH_Handle-4Q-Subscriber_BaseClass.contents.vi"/>
</Library>
