﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="PH_SequencerState-Follower_BaseClass.i attribute.ctl" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.i attribute.ctl"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.i attribute.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.i attribute.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.set i Attr.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.set i Attr.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="PH_SequencerState-Follower_BaseClass.get i attribute.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.get i attribute.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.PC Seq SM.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.PC Seq SM.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.ProcCases.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.ProcCases.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.ProcEvents.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.ProcEvents.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.ProcPeriodic.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.ProcPeriodic.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.set GUI Refs.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.set GUI Refs.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.set GUI Elements.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.set GUI Elements.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.set i attribute.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.set i attribute.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.get Information.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.get Information.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="PH_SequencerState-Follower_BaseClass.constructor.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.constructor.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.destructor.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.destructor.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.get data to modify.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.get data to modify.vi"/>
		<Item Name="PH_SequencerState-Follower_BaseClass.set modified data.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.set modified data.vi"/>
	</Item>
	<Item Name="PH_SequencerState-Follower_BaseClass.contents.vi" Type="VI" URL="../PH_SequencerState-Follower_BaseClass.contents.vi"/>
</Library>
